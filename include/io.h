#ifndef IO_H
#define IO_H


/*
	outb		- Send byte of data 
	@param port	- port to send data to
	@param data - byte to send to port
*/
void outb(unsigned short port, unsigned char data);

/*
	inb			- Get byte of data from port
	@param port	- Port to get data from
*/
unsigned char inb(unsigned short port);
#endif /* IO_H */