#ifndef SERIAL_H
#define SERIAL_H

#define PORT 0x3F8

/*
	serialinit	- set up serial
*/
void serialinit();

/*
	serialread	- get data from serial
	returns incoming value of serial
*/
char serialread();

/*
	serialstr			- Write string to serial
	@param buffer		- string to write to serial
*/
void serialstr(char* buffer);

#endif