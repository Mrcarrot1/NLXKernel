# NLXKernel
###### By PlanetAaron and 0xFF
NLX is a lightweight unix-like kernel made for x86 processors (support for other architextures is planned)
While the final kernel will be 64 bit only, there is a 32 bit branch for testing if needed. 


## Compiling

**Linux**
For this, you will need a toolchain with gcc built for cross compilation. You can build it yourself, or just download it from https://mirrors.edge.kernel.org/pub/tools/crosstool/files/bin/x86_64/8.1.0/
extract the tar file, put the outputted folder somewhere and add the bin folder in it to your path. As long as your prefix is "x86_64-linux" it should work. If not, something like this should work
```
make TOOLCHAIN=prefix_here
```
You will also need nasm. How you get it depends on your distro. QEMU is also recommended.

To build the iso, you will need grub installed on your system (most people do) as well as xorriso. If your distro's package has the prefix for grub tools as grub rather than grub2, you might need to symlink or edit the makefile.
I promist I will make this easier in the future

**MacOS/Windows**
I will not be supporting building on these.
While I used to have info on building on MacOS, I removed them because I was using grub's mkrescue tool to make an iso and I don't know if that can be done on Mac.
As for Windows, I don't really use it so if I were to look into building on it, it might work if I change the build system (which I will likely do).

## Testing
Install qemu on your system. 
After that, run this
```make run```


## TODO
- [X] Call Stack
- [X] Sending data to I/O ports
- [X] Writing to framebuffer
- [X] Serial Driver
- [X] Enter Long Mode (64 bit support)
- [ ] Keyboard Driver
- [ ] Filesystem Driver 
- [ ] Basic Userland support
- [ ] Basic Bootloader to load it 
