#include <utils.h>
#include <framebuffer.h>
#include <serial.h>

int kmain()
{
	serialinit();
	clear_fb();
	pos_reset();
	/* hacker voice - we're in */
	/* print some information */
	display_print_string("NLX\nBuilt on: " __DATE__ " at " __TIME__ "\n", WHITE, BLACK);
	
	/* print it over serial as well */
	serialstr("NLX\nBuilt on:" __DATE__ " at " __TIME__ "\n");
	return(0);
}
