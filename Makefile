TOOLCHAIN:= x86_64-linux
CC := $(TOOLCHAIN)-gcc
LD := $(TOOLCHAIN)-gcc
AS := nasm -f elf64
CFLAGS =-Wall -Wextra -Wno-unused-variable -Werror -c -I./include -ffreestanding -mcmodel=large -mno-red-zone -mno-sse -mno-sse2
LDFLAGS := -T link.ld -nostdlib -lgcc -n
ASFLAGS := 
# the fate of the world relies on those backslashes being aligned
# don't fuck it up sMH
# no i'm serious you have *one* fucking job, i swear if those get misaligned
# i'm going to scream or something
OBJ =	asm/multiboot.o 		\
		asm/longmode.o			\
		asm/io.o				\
		asm/loader.o			\
		source/framebuffer.o	\
		source/main.o			\
		source/serial.o			\
		source/utils.o

all: kernel.elf

kernel.elf: $(OBJ)
	$(LD) $(LDFLAGS) $(OBJ) -o kernel.elf

os.iso: kernel.elf
	cp kernel.elf bootimage/boot/kernel.elf
	# oh yeah don't fuck up these backslashes either
	# or else.
	grub2-mkrescue -o os.iso bootimage

run: os.iso
	qemu-system-x86_64 -cdrom os.iso -serial stdio

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@

%.o: %.S
	$(AS) $(ASFLAGS) $< -o $@

clean:
	rm -rf *.o *.elf
	rm -rf source/*.o
	rm -rf asm/*.o
	rm -rf os.iso
